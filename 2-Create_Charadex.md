# Create Charadex

We'll set some _very_ basic Charadex functionality so we have something to connect a lambda to.

Since we haven't set up the database yet, we can really only return hard-coded values.

## Define the Character

Create a `src` directory in the root of the project.

Barrel imports can keep your import statements cleaner. So it's a good habbit to create an `index.ts` file in all directories. Make that now, but leave it blank. We'll fill it in later.

Also create a `character.ts` file. In this file we'll define the character.

```typescript
export interface Character {
    /**
     * System generated ID for the character
     */
    id: string

    /**
     * Name of the character
     */
    name: string

    /**
     * userId/email address of the user who plays this character
     */
    player: string

    /**
     * System the character is designed for.
     *
     * eg: DND-5E, DND-3.5, Pathfinder, Numenera
     */
    system: string
}
```

Now that this interface exists, we can export it from our index file. Add this line to the `index.ts` file created earlier.

```typescript
export * from './character'
```

## Create Charadex

Finally lets create the Charadex service. In a full production system we'd probably create an interface and separate implementation. For simplicity in this project though we'll just create a class.

Create a file named `charadex.ts` in the `src` directory. Then add this code to that new file.

```typescript
import {Character} from './character';

export class Charadex {
    async getCharacter(id: string): Promise<Character> {
       return {
            id: id,
            name: '',
            player: '',
            system: ''
        }
    }
}
```

Why is the function async?

This is using `async` because eventually we'll be reading this out of a database. So while we don't have any code that benefits from `await` yet, setting this up as an async function now just makes it ready for when we fill out the functionality later.

The rest of this is just a stub so that the lambda we set shortly will have something to return.

Finally, don't forget to export this file in the `index.ts`.

```typescript
export * from './charadex'
```

## Unit Tests

We don't have much code, but we have enough to begin setting up our unit tests.

In the `src` directory create `charadex.spec.ts`.

The content for this file is:

```typescript
import {Charadex} from './charadex';

describe('Charadex', () => {
    describe('getCharacter', () => {
        it('should return a Character with the ID provided', async () => {
            const charadex = new Charadex()

            const character = await charadex.getCharacter('123')

            expect(character.id).toEqual('123')
        })
    })
})
```

The first `describe` just notates the class we're testing. The second `describe` is to group our tests by the function being tested. This helps keep the testing out put clean.

Notice that the callback function in the `it` function has `async` infront of it. This is important because our `getCharacter` function returns a Promise. This ensures
the promise actually resolves before the `expect` case is evaluated. If you forget this `async` keyword, the test becomes a race condition and you will probably have intermittent failures which can be tricky to debug.

With this in place you should be able to run `npm test` and see output similar to this:

```
 PASS  src/charadex.spec.ts
  Charadex
    getCharacter
      ✓ should return a Character with the ID provided (1 ms)

Test Suites: 1 passed, 1 total
Tests:       1 passed, 1 total
Snapshots:   0 total
Time:        1.49 s, estimated 2 s
Ran all test suites.
```
