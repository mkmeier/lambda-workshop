# First Lambda

## The Handler function

The meat and potatoes of AWS Lambda is the handler function. This is how the Lambda service interacts with our code. Once we have everything setup, when we send an HTTP request to AWS API Gateway, it will convert our request to a Lambda Event. Lambda will in turn activate our Lambda Function, then pass the event to our Handler function. It's our responsibility to define how we're going to handle and respond to that event.

To keep our lambda handlers together create a new directory: `src/lambda`

We don't need to create an `index.ts` file here because we won't be importing them anywhere else in our project.

To keep things organized we'll use one lambda per endpoint. The only functionality we have is the ability to get a character. So create `getCharacter.ts` in the `lambda` directory.

Before we write some code. Lets import lambda type information so intellisense helps us in our IDE.
```
npm i -D @types/aws-lambda
```

This types library lets us use AWS Interfaces when defining our handler function, and makes it easier for us to avoid using the dreaded `any` type. 

Alright, lets add some code to our `getCharacter.ts` file.


```typescript
import {APIGatewayEvent, APIGatewayProxyResult, Handler} from 'aws-lambda';


export const handle: Handler = async (event: APIGatewayEvent): Promise<APIGatewayProxyResult> => {
  // TODO: Implement Handler
}
```

This is just the stub for any handler function. We'll need to invoke Charadex from here. But where we put it in this file is important. Rather than setting up Charadex on every invocation, it would be better if once it was set up, every other time the lambda is invoked we can just reuse the same object.

We can achieve this by declaring Charadex outside the handler function. Variables outside the handler are carried over when the lambda is reused. We can either initalize it as a one-line function so that it's created when the module is loaded: 

```typescript
import {APIGatewayEvent, APIGatewayProxyResult, Handler} from 'aws-lambda';
import {Charadex} from '../charadex';

const charadex = new Charadex()

export const handle: Handler = async (event: APIGatewayEvent): Promise<APIGatewayProxyResult> => {
  // TODO: Implement Handler
}
```

Or we can just define the variable, and lazy-load it in the handler function. Both are fine, but for more complex objects, the lazy-load can be more flexible. So lets go for that route in this implementation.

```typescript
import {APIGatewayEvent, APIGatewayProxyResult, Handler} from 'aws-lambda';
import {Charadex} from '../charadex';

let charadex: Charadex

export const handle: Handler = async (event: APIGatewayEvent): Promise<APIGatewayProxyResult> => {
    if (!charadex) {
        charadex = new Charadex()
    }    
    
    // TODO: Implement Handler
}
```

In order to call the `getCharacter` function on `Chardex` we'll need the character ID. We can get that as a path parameter. This means our HTTP call will be in the form of:

```
GET /character/{id}
```

The event that Lambda gives us has a `pathParameters` property on it. So we'll take the ID from there.


```typescript
const characterId = event.pathParameters.id
```

If you're using an ID with Intellisense, you'll likely be getting an error that `pathParameters` could be null. This is because it's not a required property. So we should add some defensive coding to validate that the lambda is invoked as we expect it to be. This is because our Typescript config has `strict: true` so we must handle this potential undefined value.

This block of code will validate that the id is actually on the path parameters, and return a 400 error if it isn't. We'll talk more about the return object later.

Include this code above where `characterId` is declared.

```typescript
if (!event.pathParameters?.id) {
    return {
        statusCode: 400,
        body: "Invalid URL: No character ID in the path."
    }
}
```

Now that we have the ID, we can invoke the Charadex service.


```typescript
    const character = await charadex.getCharacter(characterId)
```

Finally we can return this character to the caller with this code:

```typescript
    return {
        statusCode: 200,
        body: JSON.stringify(character)
    }
```

Because this lambda is designed to work with API Gateway our return object is expected to have two properties:

* `statusCode` - The HTTP status code that will be returned
* `body` - A string value for the content that will be returned. For no content this can be set to an empty string. But it must have a value.

In this case because we were successful in returning the character, we're returning a `200` status code.

For the body, we're returning a JSON representation of the character, so we can just stringify the object we have.

The basic handler is complete and should look something like this:

```typescript
import {APIGatewayEvent, APIGatewayProxyResult, Handler} from 'aws-lambda';
import {Charadex} from '../charadex';

let charadex: Charadex

export const handle: Handler = async (event: APIGatewayEvent): Promise<APIGatewayProxyResult> => {
    if (!charadex) {
        charadex = new Charadex()
    }

    if (!event.pathParameters?.id) {
        return {
            statusCode: 400,
            body: "Invalid URL: No character ID in the path."
        }
    }

    const characterId = event.pathParameters.id

    const character = await charadex.getCharacter(characterId)

    return {
        statusCode: 200,
        body: JSON.stringify(character)
    }
}
```

To invoke this we'll need to setup the Serverless Framework.

## Serverless Framework

We'll setup our Lambda's with the Serverless Framework. Couple reasons for this choice:

* Handles compiling and uploading of our code.
* Handles creating/updating all the lambdas in AWS.
* Uses AWS CloudFormation to provision the lambdas, API Gateway, and other AWS Resources.
* Has plugins that allow for simulating AWS locally.

To use this frame work we need a few more dependencies. Run this command to install them.

```
npm i -D serverless serverless-offline serverless-plugin-typescript
```

* `serverless` -- The actual framework
* `serverless-offline` -- A plugin for Serverless that simulate running lambdas locally.
* `serverless-plugin-typescript` -- The lambda runtime is Javascript, so our Typescript must be compiled to Javascript before it is uploaded to AWS. This plugin automates that process.

In the root of your project create a file named `serverless.yml`. Then copy/paste this code into it.


```yml
service: charadex

plugins:
  - serverless-offline
  - serverless-plugin-typescript

provider:
  name: aws
  runtime: nodejs16.x

functions:
    getCharacter:
    handler: src/lambda/getCharacter.handle
    events:
      - http:
          path: /character/{id}
          method: get
```

Lets go over this piece-by-piece to understand what it's doing.

### Service

```yml
service: charadex
```

This is just defining the name of our service. Serverless will use this to name the CloudFormation stack when we eventually deploy this to AWS.

### Plugins

```yml
plugins:
  - serverless-offline
  - serverless-plugin-typescript
```

This block just tells Serverless that we want to use the plugins that we installed.

### Provider

```yml
provider:
  name: aws
  runtime: nodejs16.x
```

The `name` value tells Serverless that we're deploying to AWS. Serverless also supports Google Cloud and Microsoft Azure. But that's outside the scope of this demo.

Other values in `provider` are global so we don't have to re-define them on all our Lambda functions. In this case `runtime: nodejs16.x` just defines that all our functions will use the Node 16 runtime. AWS Lambda supports many different runtimes for the various languages it supports. A full list can be found here: https://docs.aws.amazon.com/lambda/latest/dg/lambda-runtimes.html


### Functions

```yml
functions:
```

This block defines each of our functions. We'll continue to add to this section as we define additional lambdas.

```yml
getCharacter:
```
This line is arbitrary. It just needs to be unique in the `functions` block. Serverless will use this to give our lambda function a name.

```yml
handler: src/lambda/getCharacter.handle
```

This is where we tell Serverless where the handler function is located. This is the path from the root of our project. We can name our handler function anything.  The value after the period is just the name of the exported function, so it's possible for a single file to define multiple handlers.

```yml
events:
  - http:
      path: /character/{id}
      method: get
```

Lambda can be invoked by many different event sources. `http` is the signal to Serverless that we want this respond to HTTP requests through API Gateway. Because we have this block here, Serverless will infer that API Gateway is needed, and will automatically create it and connect it to our Lambda.

`path:` defines the endpoint. Specific to this endpoint we're using curly braces around the ID. The curly braces is what defines `id` as our path parameter. The value inside the braces defines what the name of the path parameter, which is why `event.pathParameters.id` retrieves this value.

## Running The App

Now that everything is wired together we can run it locally. To simply this lets at another node script.

Go back to the `package.json` file and add this line in the `scripts` block.

```json
"start": "sls offline"
```

The complete block should look like:

```json
"scripts": {
  "start": "sls offline",
  "test": "jest"
},
```

Now run `npm start` and you should see the following output:

```
> charadex@0.0.0 start
> sls offline

Compiling with Typescript...
Using local tsconfig.json - tsconfig.json
Typescript compiled.
Watching typescript files...

Starting Offline at stage dev (us-east-1)

Offline [http for lambda] listening on http://localhost:3002
Function names exposed for local invocation by aws-sdk:
           * getCharacter: charadex-dev-getCharacter

   ┌────────────────────────────────────────────────────────────────────────────────┐
   │                                                                                │
   │   GET | http://localhost:3000/dev/character/{id}                               │
   │   POST | http://localhost:3000/2015-03-31/functions/getCharacter/invocations   │
   │                                                                                │
   └────────────────────────────────────────────────────────────────────────────────┘

Server ready: http://localhost:3000 🚀

```
So what happend? Serverless-Offline did a whole bunch of things for us:

1. Compile our Typescript to Javascript.
2. Wrap our application in an express-js server.
3. Emulates the Lambda API so that when we invoke our endpoint on the express server it behaves like Lambda and API Gateway.

To test the code you can invoke the HTTP Endpoint displayed in the output above. You can use Postman or any other tool you like. If you don't have Postman, just open a new terminal window and use this curl command:

```
curl --location --request GET 'http://localhost:3000/dev/character/abc'
```

The response should look like this:

```
{"id":"abc","name":"","player":"","system":""}
```

In the terminal where our code is running you can see some basic logs showing that it was invoked.

```
GET /dev/character/abc (λ: getCharacter)
(λ: getCharacter) RequestId: 29dc4e86-698c-4fc4-83ae-f16bb0b24e7e  Duration: 49.01 ms  Billed Duration: 50 ms
```

If you log anything with `console.log` it will also be displayed here.

For example. If we were trying to troubleshoot or debug something about the event that is invoking our lambda we could add this line to our lambda handler:

```typescript
console.log('Event: ', event)
```

The event is rather large, so it's not recommended to leave code like this in. But it can be very helpful for trouble shooting. The contents of this log isn't important right now. I'm just posting the content for awareness as a troubleshooting aid.

```
GET /dev/character/abc (λ: getCharacter)
Event:  {
  body: null,
  headers: {
    'User-Agent': 'PostmanRuntime/7.29.2',
    Accept: '*/*',
    'Postman-Token': '5ccd6318-e13e-459a-af1a-8a41d06ae9ef',
    Host: 'localhost:3000',
    'Accept-Encoding': 'gzip, deflate, br',
    Connection: 'keep-alive'
  },
  httpMethod: 'GET',
  isBase64Encoded: false,
  multiValueHeaders: {
    'User-Agent': [ 'PostmanRuntime/7.29.2' ],
    Accept: [ '*/*' ],
    'Postman-Token': [ '5ccd6318-e13e-459a-af1a-8a41d06ae9ef' ],
    Host: [ 'localhost:3000' ],
    'Accept-Encoding': [ 'gzip, deflate, br' ],
    Connection: [ 'keep-alive' ]
  },
  multiValueQueryStringParameters: null,
  path: '/character/abc',
  pathParameters: { id: 'abc' },
  queryStringParameters: null,
  requestContext: {
    accountId: 'offlineContext_accountId',
    apiId: 'offlineContext_apiId',
    authorizer: {
      claims: undefined,
      principalId: 'offlineContext_authorizer_principalId',
      scopes: undefined
    },
    domainName: 'offlineContext_domainName',
    domainPrefix: 'offlineContext_domainPrefix',
    extendedRequestId: '75951121-39f7-4e85-97a5-61ad159ad0ce',
    httpMethod: 'GET',
    identity: {
      accessKey: null,
      accountId: 'offlineContext_accountId',
      apiKey: 'offlineContext_apiKey',
      apiKeyId: 'offlineContext_apiKeyId',
      caller: 'offlineContext_caller',
      cognitoAuthenticationProvider: 'offlineContext_cognitoAuthenticationProvider',
      cognitoAuthenticationType: 'offlineContext_cognitoAuthenticationType',
      cognitoIdentityId: 'offlineContext_cognitoIdentityId',
      cognitoIdentityPoolId: 'offlineContext_cognitoIdentityPoolId',
      principalOrgId: null,
      sourceIp: '127.0.0.1',
      user: 'offlineContext_user',
      userAgent: 'PostmanRuntime/7.29.2',
      userArn: 'offlineContext_userArn'
    },
    operationName: undefined,
    path: '/character/abc',
    protocol: 'HTTP/1.1',
    requestId: 'cc3dd5ca-6162-4255-922a-43f73b4a6b5a',
    requestTime: '11/Sep/2022:03:41:05 -0500',
    requestTimeEpoch: 1662885665750,
    resourceId: 'offlineContext_resourceId',
    resourcePath: '/dev/character/{id}',
    stage: 'dev'
  },
  resource: '/character/{id}',
  stageVariables: null
}
(λ: getCharacter) RequestId: 3939ebbc-a3ae-4dba-8975-3b004b4805c6  Duration: 53.43 ms  Billed Duration: 54 ms
```
