# Deploying to AWS

Now that our first lambda is defined. Lets deploy it to the cloud.

With serverless installed all we have to do is execute this command:

```
npx sls deploy
```

It will take a bit to execute the first time. But you should see output similar to this:


```
Deploying charadex to stage dev (us-east-1)
Compiling with Typescript...
Using local tsconfig.json - tsconfig.json
Typescript compiled.

✔ Service deployed to stack charadex-dev (122s)

endpoint: GET - https://37vppkfxyc.execute-api.us-east-1.amazonaws.com/dev/character/{id}
functions:
  getCharacter: charadex-dev-getCharacter (99 kB)
```

What is this telling us?

```
Service deployed to stack charadex-dev
```

This means Serverless created a CloudFormation stack named `charadex-dev`. We'll check this out in the console later.

```
endpoint: GET - https://37vppkfxyc.execute-api.us-east-1.amazonaws.com/dev/character/{id}
```

Serverless created an API Gateway with the ID `37vppkfxyc` (yours will be different). This is the path we can use to invoke our lambda.

You can test it just like we did for local testing in the previous section. For example:

```
❯ curl -v --location --request GET 'https://37vppkfxyc.execute-api.us-east-1.amazonaws.com/dev/character/asdf'
{"id":"asdf","name":"","player":"","system":""}%  
```

Lets head to the AWS Console and check out what happened.

# CloudFormation

CloudFormation is the service AWS uses for managing Serverless applications. Serverless converts our `serverless.yml` into a CloudFormation template file that defines what resources we want to create, and how they are related to each other.

To view our stack. Open up the AWS Console, and search for the `CloudFormation` service. If you don't see any stacks here, double check the region you're viewing. By default Serverless will deploy to `us-east-1`. But the output of `sls deploy` lists the region it's deploying to. Make sure you're in the same region in the console.

Serverless is doing a lot for us. It's not important to understand this now, but just as an illistration click on the name of your stack `charadex-dev` in our case. Then go to the `Resources` tab. 

This tab lists all the resources that Serverless defined for us based on our `serverless.yml` file. The highlights are:

* `ApiGatewayRestApi` - The API Gateway that gives us a URL to hit.
* `ApiGatewayMethodCharacterIdVarGet` - The HTTP Method/Path configured inside API Gateway that connects to our lambda.
* `GetCharacterLambdaFunction` - Our Lambda Function.
* `IamRoleLambdaExecution` - IAM Role that gives our Lambda permission to interact with API Gateway.
* `ServerlessDeploymentBucket` - The S3 bucket where our code is uploaded to.

This is also a handy place to go to access the specific resources created.

The line with `GetCharacterLambdaFunction` has a link to our lambda function. Click on the link.

# Lambda

In the Lambda console we'll see our code. It will look different than what we wrote because it's not Typescript. This is the Javascript that was produced by our Typescript code.

If you tested the function while it was deployed there should be logs we can look at.

Go to the Monitor tab. Then click on `View logs in CloudWatch`. Click on `Search log group`. This shows logs across all log streams. This is where you would find logs produced by your function. If you don't see anything, it could be the time interval is too short to include your calls. Use the time interval buttons in the upper right to broaden the search.