
# Setup the project

## Initialize the project 
```
npm init -y
```

This creates the `package.json` file for the project.

## Install dev dependencies

```
npm i -D typescript jest ts-jest @types/jest @types/node
```

This installs basic development dependencies we'll use in this project.

* `typescript` -- the language compiler
* `jest` -- unit testing framework
* `ts-jest` -- simplifies configuring Jest to work with Typescript
* `@types/*` -- Node and Jest are distributed as javascript. This installs type definition files so they can be understood by the Typescript compiler

## Configure the project

Create a file name `tsconfig.json` in the root of the project, and copy this config into it.

```javascript
{
  "compilerOptions": {
    "target": "es2021",
    "module": "commonjs",
    "sourceMap": true,
    "outDir": "dist",
    "esModuleInterop": true,
    "forceConsistentCasingInFileNames": true,
    "strict": true,
    "skipLibCheck": true
  },
  "include": ["src/**/*"],
  "exclude": ["node_modules", "**/*.spec.ts"]
}
```

## Configure Unit Tests

Create the Jest configuration by running this command:

```
npx ts-jest config:init
```

Then update the `scripts` section of your `package.json` to look like this:

```javascript
  "scripts": {
    "test": "jest"
  }
```

This enables it so that you can run your unit test by calling `npm test`
